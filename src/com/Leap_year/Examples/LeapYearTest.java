package com.Leap_year.Examples;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;


class LeapYearTest {

	@Test
	void test() {
		LeapYearClass lp= new LeapYearClass(2000);
		int result = lp.CheckLeapYear();
		assertEquals(result,1);
	}
	
	@Test
	void test1() {
		LeapYearClass lp= new LeapYearClass(1700);
		int result = lp.CheckLeapYear();
		assertEquals(result,1);
	}
	
	@Test
	void test2() {
		LeapYearClass lp= new LeapYearClass(1800);
		int result = lp.CheckLeapYear();
		assertEquals(result,1);
	}
	
	@Test
	void test3() {
		LeapYearClass lp= new LeapYearClass(1900);
		int result = lp.CheckLeapYear();
		assertEquals(result,1);
	}
	
	@Test
	void test4() {
		LeapYearClass lp= new LeapYearClass(2100);
		int result = lp.CheckLeapYear();
		assertEquals(result,1);
	}

}
